#!/bin/bash

export CURRENT_UID=$(id -u):$(id -g)
(
  set -x
  docker compose down
)
